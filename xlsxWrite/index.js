const path = require('path');
const XLSX = require('xlsx');

exports.runExample = () => {
  console.log('Create xlsx file.');
  console.log('...');

  // create new workbook
  const wb = XLSX.utils.book_new();

  // create an empty worksheet object
  const ws = {};

  // define cells range to be processed
  // if cells range does not defined, data will never wrote
  ws['!ref'] = `A1:B2`;

  const randomValue = Math.round(Math.random(0, 1000) * 1000);

  // set cell value
  ws.A1 = { v: `Value ${randomValue}` };
  ws.B2 = { f: `=5+${randomValue}` };

  // append worksheet into workbook and set its name
  XLSX.utils.book_append_sheet(wb, ws, 'TestSheet');

  // write to file
  const xlsxTargetPath = `${__dirname}${path.sep}result${path.sep}zzz.xlsx`;
  XLSX.writeFile(wb, xlsxTargetPath);

  console.log('Create xlsx file done!.\n');
};