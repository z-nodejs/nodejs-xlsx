const xlsxRead = require('./xlsxRead');
const xlsxReadUpdate = require('./xlsxReadUpdate');
const xlsxWrite = require('./xlsxWrite');

console.log('Running example...');
console.log('------------------');

runExample();

console.log('---------------------');
console.log('Running example done!');

function runExample () {
  xlsxRead.runExample();
  xlsxWrite.runExample();
  xlsxReadUpdate.runExample();
}
