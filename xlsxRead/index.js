const path = require('path');
const XLSX = require('xlsx');

exports.runExample = () => {
  console.log('Read xlsx file.');
  console.log('---------------');

  const xlsxSourcePath = `${__dirname}${path.sep}source${path.sep}zzz.xlsx`;

  // read xlsx file
  const wb = XLSX.readFile(xlsxSourcePath);

  // read worksheet
  const ws = wb.Sheets.Sheet1;

  // define cells range
  // if cells range does not defined, it will read all data in the sheet
  // ws['!ref'] = `A1:B4`;

  // read sheet data as JSON object
  const dataJson = XLSX.utils.sheet_to_json(ws);
  console.log('read sheet data as JSON object:');
  dataJson.forEach(row => {
    console.log(`ID: ${row.ID}, Name: ${row.Name}`);
  });
  console.log('------------------------------------');

  // read sheet data as csv
  const dataCsv = XLSX.utils.sheet_to_csv(ws, { FS: ';', RS: '\n', blankrows: false })
    .split('\n')
    .filter(item => '' !== item);
  console.log('read sheet data as CSV:');
  dataCsv.forEach(row => {
    console.log(row);
  });

  console.log('---------------------');
  console.log('Read xlsx file done!.\n');
};