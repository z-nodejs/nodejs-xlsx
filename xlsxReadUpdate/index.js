const path = require('path');
const XLSX = require('xlsx');

exports.runExample = () => {
  console.log('Read update xlsx file.');
  console.log('...');

  const xlsxSourcePath = `${__dirname}${path.sep}source${path.sep}zzz.xlsx`;

  // read xlsx file
  const wb = XLSX.readFile(xlsxSourcePath);

  // read worksheet
  const ws = wb.Sheets.Sheet1;

  // define cells range to be processed
  // if cells range does not defined, new cell value which set out of current range will never affected
  ws['!ref'] = `A1:B2`;

  const randomValue = Math.round(Math.random(0, 1000) * 1000);

  // update cell value
  ws.A1 = { v: `Value ${randomValue}` };
  ws.B2 = { f: `=5+${randomValue}` };

  // write to file
  XLSX.writeFile(wb, xlsxSourcePath);
  console.log('Read update xlsx file done!.\n');
};