const cellColMaxLength = 3;
// value of XFD
const maxColVal = 15681;
const charCodeOfA = 'A'.charCodeAt(0);
const charCodeAt0MaxVal = 26;
const charCodeAt1MaxVal = Math.pow(charCodeAt0MaxVal, 2);

exports.isValidXlsxCell = isValidXlsxCell = (cellName) => {
  if (!cellName && /[^A-Z0-9]/.test(cellName)) {
    return false;
  }

  const firstDigit = cellName.search(/\d/);
  const row = cellName.substring(firstDigit, cellName.length);
  if (/[^0-9]/.test(row) || maxColVal < Number(row)) {
    return false;
  }

  const col = cellName.substring(0, firstDigit);
  if (cellColMaxLength < col.length) {
    return false;
  }

  return true;
};

exports.nextXlsxColOfCell = (cellName, range) => {
  if (!this.isValidXlsxCell(cellName)) {
    throw new Error(`${cellName} is invalid`);
  }

  const firstDigit = cellName.search(/\d/);
  const rowNum = cellName.substring(firstDigit, cellName.length);
  const colName = cellName.substring(0, firstDigit);

  let colVal;
  if (1 === colName.length) {
    colVal = colName.charCodeAt(0) - charCodeOfA;
  } else if (2 === colName.length) {
    colVal = ((colName.charCodeAt(0) - charCodeOfA - 1) * charCodeAt0MaxVal) + colName.charCodeAt(1) - charCodeOfA;
  } else {
    colVal = ((colName.charCodeAt(0) - charCodeOfA - 1) * charCodeAt1MaxVal) + colName.charCodeAt(1) - charCodeOfA;
  }

  let nextColVal = colVal + range;
  let nextXlsxCellCol;
  if (charCodeAt0MaxVal >= nextColVal) {
    nextXlsxCellCol = String.fromCharCode(nextColVal + charCodeOfA);
  } else if (charCodeAt1MaxVal >= nextColVal) {
    const charCodeAt0 = (nextColVal / charCodeAt0MaxVal) + charCodeOfA;
    const charCodeAt1 = (nextColVal % charCodeAt0MaxVal) + charCodeOfA;
    nextXlsxCellCol = `${String.fromCharCode(charCodeAt0)}${String.fromCharCode(charCodeAt1)}`;
  } else {
    const charCodeAt0 = (nextColVal / charCodeAt0MaxVal) + charCodeOfA;
    const charCodeAt1Val = nextColVal % charCodeAt1MaxVal;
    const charCodeAt1 = (charCodeAt1Val / charCodeAt1MaxVal) + charCodeOfA;
    const charCodeAt2 = (charCodeAt1Val % charCodeAt1MaxVal) + charCodeOfA;
    nextXlsxCellCol = `${String.fromCharCode(charCodeAt0)}${String.fromCharCode(charCodeAt1)}`;
    nextXlsxCellCol = `${String.fromCharCode(charCodeAt0)}${String.fromCharCode(charCodeAt1)}${String.fromCharCode(charCodeAt2)}`;
  }

  return `${nextXlsxCellCol}${rowNum}`;
};